package tutorial.thirty_days_of_code;

public class Day2_Operators {
    static void solve(double meal_cost, int tip_percent, int tax_percent) {

        double totalTip = meal_cost * tip_percent / 100;
        double totalTax = meal_cost * tax_percent / 100;
        int total = (int) Math.round(meal_cost +  totalTip + totalTax);

        System.out.print(total);

    }
}
