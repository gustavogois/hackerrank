package tutorial.thirty_days_of_code;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Day2_OperatorsTest {

    @Test
    public void solve() {

        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));

        double mealCost = 12;
        int tipPercent = 20;
        int taxPercent = 8;

        Day2_Operators.solve(12, 20, 8);

        assertEquals("15", outContent.toString());
    }
}